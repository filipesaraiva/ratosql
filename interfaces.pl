%
% This file is part of RAtoSQL.
%
% RAtoSQL is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% RAtoSQL is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
%
% Please see the file COPYING for the full license text of the GNU General Public
% License, version 3.
%
% Copyright (c) 2008-2010,
%   Filipe Saraiva
%   Herdeson Monte
%

iniciar :-
  new(Janela, dialog('Compilador AlgebraToSql')), 
      send_list(Janela, append,
		[
		  new(Formula, text_item(fórmula)),
		  new(Botao, button(inserir_Fórmula,
		    message(@prolog, inicio,
		      Formula?selection)))
	/*	  new(TextoLexico, text('Analizador Léxico')),
		  new(VLexico, view('Visualizador', size(80, 5))),
		  new(TextoSQL, text('Código SQL')),
		  new(VSql, view('Visualizador', size(80, 15))),
		  new(TextoErro, text('Mensagens de Erro e Alertas')),
		  new(VErro, view('Visualizador', size(80, 5)))
	*/	]),
      send(Janela, open).

mostraResultadoLexico:-
	new(D, dialog('Lexemas')),
	new(V, view('')),
	new(BotaoSair, button(ok, and(
		message(D, destroy)))
		),
	send(V, load('lexemas.txt')),
	send(D, append, V),
	send(D, append, BotaoSair),
	send(D, open).

mostraResultadoErro:-
	new(D, dialog('Erros Lexicos')),
	new(V, view('')),
	new(BotaoSair, button(ok, and(
		message(D, destroy)))
		),
	send(V, load('erros.txt')),
	send(D, append, V),
	send(D, append, BotaoSair),
	send(D, open).

mostraErro:-
	new(D, dialog('Erros Sintaticos')),
	new(V, view('')),
	new(BotaoSair, button(ok, and(
		message(D, destroy)))
		),
	send(V, load('erros.txt')),
	send(D, append, V),
	send(D, append, BotaoSair),
	send(D, open).

mostraCodigo:-
	new(D, dialog('Codigo Gerado')),
	new(V, view('')),
	new(BotaoSair, button(ok, and(
		message(D, destroy)))
		),
	send(V, load('codigo.txt')),
	send(D, append, V),
	send(D, append, BotaoSair),
	send(D, open).